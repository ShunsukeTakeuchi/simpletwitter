package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService{

	public void insert(Message message){

		Connection connection = null;
		try{
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void delete(String messageId){

		Connection connection = null;
		try{
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String start, String end){
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try{
			connection = getConnection();

			// startがある場合時刻を追加、ない場合デフォルト値を指定
			if(StringUtils.isBlank(start)){
				start = "2020/01/01 00:00:00";
			}else{
				start = start + " 00:00:00";
			}

			// endがある場合時刻を追加、ない場合現在時刻を指定
			if(StringUtils.isBlank(end)){
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Calendar calendar = Calendar.getInstance();
				end = format.format(calendar.getTime());
			}else{
				end = end + " 23:59:59";
			}

			// idをnullで初期化、ServletからuserIdが渡ってきた場合型変換してidに代入
			Integer id = null;
			if(!StringUtils.isEmpty(userId)){
				id = Integer.parseInt(userId);
			}

			// idがnullのとき全ユーザー、null以外のとき該当するユーザーのつぶやきを取得
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
			commit(connection);

			return messages;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public Message select(int messageId){

		Connection connection = null;
		try{
			connection = getConnection();

			Message messages = new MessageDao().select(connection, messageId);
			commit(connection);

			return messages;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void update(Message message){

		Connection connection = null;
		try{
			connection = getConnection();
			new MessageDao().update(connection, message);
			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}
}
